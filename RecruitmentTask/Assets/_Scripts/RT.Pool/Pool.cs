﻿using UnityEngine;
using Zenject;

namespace RT.ObjectPooling
{
    public class Pool<T> : MonoMemoryPool<Transform, T> where T : BasePoolableObject
    {
        private Transform originalParent;
        
        protected override void OnCreated(T item)
        {
            originalParent = item.transform.parent;
        }

        protected override void Reinitialize(Transform p1, T item)
        {
            item.PrepareForActivate(p1);
        }

        protected override void OnDespawned(T item)
        {
            item.PrepareForDeactivate();
            item.transform.SetParent(originalParent);
        }
    }
}
