﻿using UnityEngine;

namespace RT.ObjectPooling
{
    public class BasePoolableObject : MonoBehaviour
    {
        public virtual void PrepareForActivate(Transform parent)
        {
            transform.SetParent(parent);
            gameObject.SetActive(true);
        }

        public virtual void PrepareForDeactivate()
        {
            gameObject.SetActive(false);
        }
    }
}
