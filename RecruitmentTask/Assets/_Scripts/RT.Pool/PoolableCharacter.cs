﻿using System.Collections;
using System.Collections.Generic;
using RT.Ballistic;
using RT.Fire;
using RT.Movement;
using UnityEngine;

namespace RT.ObjectPooling
{
    public class PoolableCharacter : BasePoolableObject    
    {
        [SerializeField] private CharacterMovementSystem movementSystem;
        [SerializeField] private BallisticLineDrawer lineDrawer;
        [SerializeField] private BoxCollider boxCollider;
        
        public override void PrepareForActivate(Transform parent)
        {
            base.PrepareForActivate(parent);
            boxCollider.enabled = true;
        }

        public override void PrepareForDeactivate()
        {
            boxCollider.enabled = false;
            base.PrepareForDeactivate();
        }
        
        public CharacterMovementSystem GetMovementSystem()
        {
            return movementSystem;
        }

        public BallisticLineDrawer GetLineDrawer()
        {
            return lineDrawer;
        }

        public BaseCharacterFightSystem GetCharacterFightSystem()
        {
            return lineDrawer.GetCharacterFightSystem();
        }

        public BoxCollider GetBoxCollider()
        {
            return boxCollider;
        }
    }
}
