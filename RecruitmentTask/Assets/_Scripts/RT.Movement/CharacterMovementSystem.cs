﻿using RT.Input;
using UnityEngine;
using DG.Tweening;

namespace RT.Movement
{
    public class CharacterMovementSystem : MonoBehaviour
    {
        [SerializeField]
        private Transform playerChar;
        private IInputSystemFacade inputSystemFacade;

        public void InitializeSystem(IInputSystemFacade inputSystemFacade)
        {
            inputSystemFacade.AddListener<Drag>(InputAction.Performed, UpdatePosition);
            this.inputSystemFacade = inputSystemFacade;
        }

        public void DisposeSystem()
        {
            inputSystemFacade.RemoveListener<Drag>(InputAction.Performed, UpdatePosition);
            this.inputSystemFacade = null;
        }
        
        private void UpdatePosition()
        {
            var yAngle = playerChar.localEulerAngles.y;
            //just line function -7x + 8, where x = [0;1]. Hence rotation value is between 1 and 8 depending on player finger y axis pos on the screen.
            yAngle += (-7 * inputSystemFacade.GetYDragTouchPoint() + 8) * -inputSystemFacade.GetDragSign();
            var endRot = new Vector3(0f, yAngle, 0f);
            playerChar.DOLocalRotate(endRot, .1f);
        }
    }
}
