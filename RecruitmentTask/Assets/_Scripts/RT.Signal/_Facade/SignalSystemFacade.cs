﻿using System;

namespace RT.Signal
{
    public class SignalSystemFacade : ISignalSystemFacade
    {
        private readonly ISignalSystem<CustomSignal> signalSystem;

        public SignalSystemFacade(ISignalSystem<CustomSignal> signalSystem)
        {
            this.signalSystem = signalSystem;
        }
        
        public void FireSignal<T>() where T : CustomSignal
        {
            signalSystem.FireSignal<T>();
        }

        public void FireSignal<T>(T signal) where T : CustomSignal
        {
            signalSystem.FireSignal<T>(signal);
        }
        
        public void SubscribeSignal<T>(Action onFire) where T : CustomSignal
        {
            signalSystem.SubscribeSignal<T>(onFire);
        }

        public void SubscribeSignal<T>(Action<T> onFire) where T : CustomSignal
        {
            signalSystem.SubscribeSignal<T>(onFire);
        }
        
        public void UnsubscribeSignal<T>(Action onFire) where T : CustomSignal
        {
            signalSystem.UnsubscribeSignal<T>(onFire);
        }

        public void UnsubscribeSignal<T>(Action<T> onFire) where T : CustomSignal
        {
            signalSystem.UnsubscribeSignal<T>(onFire);
        }
    }
}
