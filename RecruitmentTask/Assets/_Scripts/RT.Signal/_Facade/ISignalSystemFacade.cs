﻿using System;
namespace RT.Signal
{
    public interface ISignalSystemFacade
    {
        void SubscribeSignal<T>(Action onFire) where T : CustomSignal;
        void SubscribeSignal<T>(Action<T> onFire) where T : CustomSignal;
        void UnsubscribeSignal<T>(Action onFire) where T : CustomSignal;
        void UnsubscribeSignal<T>(Action<T> onFire) where T : CustomSignal;
        void FireSignal<T>() where T : CustomSignal;
        void FireSignal<T>(T signal) where T : CustomSignal;
    }
}
