﻿using System;

namespace RT.Signal
{
    public interface ISignalSystem<TSignal> where TSignal : class
    {
        void SubscribeSignal<T>(Action onFire) where T : TSignal;
        void SubscribeSignal<T>(Action<T> onFire) where T : TSignal;
        void UnsubscribeSignal<T>(Action onFire) where T : TSignal;
        void UnsubscribeSignal<T>(Action<T> onFire) where T : TSignal;
        void FireSignal<T>() where T : TSignal;
        void FireSignal<T>(T signal) where T : TSignal;
    }
}
