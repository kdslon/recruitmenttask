﻿using System;
using Zenject;

namespace RT.Signal
{
    public class SignalSystem : ISignalSystem<CustomSignal>
    {
        private SignalBus signalBus;

        public SignalSystem(SignalBus signalBus)
        {
            this.signalBus = signalBus;
        }

        public void SubscribeSignal<T>(Action onFire) where T : CustomSignal
        {
            signalBus.Subscribe<T>(onFire);
        }

        public void SubscribeSignal<T>(Action<T> onFire) where T : CustomSignal
        {
            signalBus.Subscribe<T>(onFire);
        }

        public void UnsubscribeSignal<T>(Action onFire) where T : CustomSignal
        {
            signalBus.TryUnsubscribe<T>(onFire);
        }

        public void UnsubscribeSignal<T>(Action<T> onFire) where T : CustomSignal
        {
            signalBus.TryUnsubscribe<T>(onFire);
        }

        public void FireSignal<T>() where T : CustomSignal
        {
            signalBus.Fire<T>();
        }

        public void FireSignal<T>(T signal) where T : CustomSignal
        {
            signalBus.Fire(signal);
        }
    }
}
