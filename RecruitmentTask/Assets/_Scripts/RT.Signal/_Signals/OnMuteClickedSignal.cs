﻿

namespace RT.Signal
{
   public class OnMuteClickedSignal : CustomSignal
   {
      public bool isMute;

      public OnMuteClickedSignal(bool isMute)
      {
         this.isMute = isMute;
      }
   }
}
