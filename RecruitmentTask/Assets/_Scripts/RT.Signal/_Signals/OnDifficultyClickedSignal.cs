﻿namespace RT.Signal
{
    public class OnDifficultyClickedSignal : CustomSignal
    {
        public bool isHardEnabled;

        public OnDifficultyClickedSignal(bool isHardEnabled)
        {
            this.isHardEnabled = isHardEnabled;
        }

    }
}
