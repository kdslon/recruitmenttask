﻿using RT.Utilities;

namespace RT.Signal
{
    public class OnGenerateLevelSignal : CustomSignal
    {
        public Level level;

        public OnGenerateLevelSignal(Level level)
        {
            this.level = level;
        }

    }
}