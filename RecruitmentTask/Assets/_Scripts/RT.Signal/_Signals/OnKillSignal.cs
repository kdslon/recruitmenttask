﻿using System.Collections;
using System.Collections.Generic;
using RT.Fire;
using UnityEngine;

namespace RT.Signal
{
    public class OnKillSignal : CustomSignal
    {
        public BaseCharacterFightSystem characterFightSystem;

        public OnKillSignal(BaseCharacterFightSystem characterFightSystem)
        {
            this.characterFightSystem = characterFightSystem;
        }
    }

}