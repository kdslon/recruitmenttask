﻿using DG.Tweening;
using RT.Input;
using RT.Signal;
using RT.Utilities;
using UnityEngine;
using Zenject;

namespace RT.Fire
{
    public class BaseCharacterFightSystem : MonoBehaviour
    {
        [Inject] 
        protected ISignalSystemFacade signalSystemFacade;
        
        [SerializeField] 
        protected CharacterType characterType;
        
        [SerializeField] 
        protected ParticleSystem fireParticle;

        [SerializeField] 
        protected ParticleSystem deathParticle;
        
        private BaseCharacterFightSystem target;
        
        public virtual void InitializeSystem(IInputSystemFacade inputSystemFacade)
        {
            inputSystemFacade.AddListener<Tap>(InputAction.Performed, Shoot);
        }
        
        public virtual void DisposeSystem(IInputSystemFacade inputSystemFacade)
        {
            inputSystemFacade.RemoveListener<Tap>(InputAction.Performed, Shoot);
        }
        
        public void UpdateTarget(BaseCharacterFightSystem target)
        {
            this.target = target;
        }
        
        public CharacterType GetCharacterType()
        {
            return characterType;
        }
        
        protected virtual void Shoot()
        {
            fireParticle.Play();   
            signalSystemFacade.FireSignal<OnShootSignal>();
            if (target == null) return;
            target.Kill();
            UpdateTarget(null);
        }
        
        protected virtual void Kill()
        {
            deathParticle.Play();
            transform.DOLocalRotate(Vector3.forward * 90f, .3f);
            signalSystemFacade.FireSignal(new OnKillSignal(this));
        }
    }
}
