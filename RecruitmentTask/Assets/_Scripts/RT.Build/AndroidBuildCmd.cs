﻿#if UNITY_EDITOR
using UnityEditor;
using System.Linq;
using System;
using UnityEditor.Build.Reporting;

namespace RT.Build
{
    public static class AndroidBuildCmd
    {
        public static void PerformBuild()
        {
            Console.WriteLine("Performing build");
            
            var buildPath = GetBuildPath();
            var buildName = GetBuildName();
            var buildOptions = GetBuildOptions();
            var fixedBuildPath = GetFixedBuildPath(buildPath, buildName);

            var buildReport = BuildPipeline.BuildPlayer(GetEnabledScenes(), fixedBuildPath, BuildTarget.Android, buildOptions);
            if (buildReport.summary.result != BuildResult.Succeeded)
                throw new Exception($"Build ended with {buildReport.summary.result} status");

            Console.WriteLine("Build ready!");
        }

        private static string GetBuildPath()
        {
            var buildPath = GetArgument("customBuildPath");
            Console.WriteLine($"BuildPath: {buildPath}");

            if (buildPath == "")
            {
                throw new Exception("customBuildPath argument is missing");
            }

            return buildPath;
        }

        private static string GetBuildName()
        {
            var buildName = GetArgument("customBuildName");
            Console.WriteLine($"BuildName: {buildName}");

            if (buildName == "")
            {
                throw new Exception("customBuildName argument is missing");
            }

            return buildName;
        }

        private static BuildOptions GetBuildOptions()
        {
            if (!TryGetEnv("BuildOptions", out var envVar)) return BuildOptions.None;
            var allOptionVars = envVar.Split(',');
            var allOptions = BuildOptions.None;
            BuildOptions option;
            string optionVar;
            var length = allOptionVars.Length;

            Console.WriteLine($":: Detecting BuildOptions env var with {length} elements ({envVar})");

            for (var i = 0; i < length; i++)
            {
                optionVar = allOptionVars[i];

                if (optionVar.TryConvertToEnum(out option))
                {
                    allOptions |= option;
                }
                else
                {
                    Console.WriteLine(
                        $":: Cannot convert {optionVar} to {nameof(BuildOptions)} enum, skipping it.");
                }
            }

            return allOptions;

        }

        private static string GetFixedBuildPath(string buildPath, string buildName)
        {
            buildName = $"{buildName}{(EditorUserBuildSettings.buildAppBundle ? ".aab" : ".apk")}";
            return $"{buildPath}{buildName}";
        }

        private static string GetArgument(string name)
        {
            var args = Environment.GetCommandLineArgs();
            var index = Array.FindIndex(args, x => x.Contains(name));
            return args[index + 1];
        }

        private static string[] GetEnabledScenes()
        {
            return
            (
                from scene in EditorBuildSettings.scenes
                where scene.enabled
                where !string.IsNullOrEmpty(scene.path)
                select scene.path
            ).ToArray();
        }

        // https://stackoverflow.com/questions/1082532/how-to-tryparse-for-enum-value
        private static bool TryConvertToEnum<TEnum>(this string strEnumValue, out TEnum value)
        {
            if (!Enum.IsDefined(typeof(TEnum), strEnumValue))
            {
                value = default;
                return false;
            }

            value = (TEnum) Enum.Parse(typeof(TEnum), strEnumValue);
            return true;
        }

        private static bool TryGetEnv(string key, out string value)
        {
            value = Environment.GetEnvironmentVariable(key);
            return !string.IsNullOrEmpty(value);
        }
    }
}
#endif