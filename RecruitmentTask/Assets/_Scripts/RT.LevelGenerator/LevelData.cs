﻿using System.Collections.Generic;
using UnityEngine;

namespace RT.LevelGenerator
{
    public class LevelData : MonoBehaviour
    {
        public Transform cameraPosition;
        public List<Transform> wallPositions;
        public List<Transform> allyPositions;
        public List<Transform> enemyPositions;
    }
}