﻿using System.Collections.Generic;
using DG.Tweening;
using RT.GameLogic;
using RT.ObjectPooling;
using RT.Signal;
using RT.Utilities;
using UnityEngine;
using Zenject;

namespace RT.LevelGenerator
{
    public class LevelGeneratorSystem : MonoBehaviour, ILevelGeneratorSystem
    {
        [Inject] 
        private readonly ILevelControllerFacade levelControllerFacade;

        [Inject] 
        private readonly Pool<PoolableAlly> allyPool;
        
        [Inject] 
        private readonly Pool<PoolableEnemy> enemyPool;
        
        [Inject] 
        private readonly Pool<PoolableWall> wallPool;

        private readonly List<PoolableAlly> spawnedAlly = new List<PoolableAlly>();
        private readonly List<PoolableEnemy> spawnedEnemy = new List<PoolableEnemy>();
        private readonly List<PoolableWall> spawnedWall = new List<PoolableWall>();

        [SerializeField] 
        private Camera mainCamera;
        
        [SerializeField]
        private List<LevelData> levels;

        [Inject]
        private ISignalSystemFacade signalSystemFacade;
        private Difficulty difficulty = Difficulty.Easy;

        private void Start()
        {
            signalSystemFacade.SubscribeSignal<OnDifficultyClickedSignal>(SetDifficulty);
            signalSystemFacade.SubscribeSignal<OnGenerateLevelSignal>(GenerateLevel);
            signalSystemFacade.SubscribeSignal<OnWinSignal>(SpawnNextLevel);
            signalSystemFacade.SubscribeSignal<OnLoseSignal>(RestartLevel);
        }

        private void GenerateLevel(OnGenerateLevelSignal signalData)
        {
            GenerateLevel(levels[(int)signalData.level]);
        }

        private void GenerateLevel(LevelData levelData)
        {
            var mainCameraTransform = mainCamera.transform;
            mainCameraTransform.position = levelData.cameraPosition.position;
            mainCameraTransform.rotation = levelData.cameraPosition.rotation;

            foreach (var val in levelData.wallPositions)
            {
                var wall = wallPool.Spawn(levelData.transform);
                var concreteWallTransform = wall.transform;
                var levelDataWallTransform = val.transform;
                concreteWallTransform.position = levelDataWallTransform.position;
                concreteWallTransform.rotation = levelDataWallTransform.rotation;
                spawnedWall.Add(wall);
            }
            
            foreach (var val in levelData.allyPositions)
            {
                var ally = allyPool.Spawn(levelData.transform);
                var concreteAllyTransform = val.transform;
                var levelDataAllyTransform = ally.transform;
                levelDataAllyTransform.position = concreteAllyTransform.position;
                levelDataAllyTransform.rotation = concreteAllyTransform.rotation;
                levelControllerFacade.AddAlly(ally);
                spawnedAlly.Add(ally);
            }
            
            foreach (var val in levelData.enemyPositions)
            {
                var enemy = enemyPool.Spawn(levelData.transform);
                var levelDataEnemyTransform = val.transform;
                var concreteEnemyTransform = enemy.transform;
                concreteEnemyTransform.position = levelDataEnemyTransform.position;
                concreteEnemyTransform.rotation = levelDataEnemyTransform.rotation;
                levelControllerFacade.AddEnemy(enemy);
                spawnedEnemy.Add(enemy);
            }
        }

        private void DespawnLevel()
        {
            foreach (var val in spawnedAlly)
            {
                allyPool.Despawn(val);
            }
            spawnedAlly.Clear();
            
            foreach (var val in spawnedEnemy)
            {
                enemyPool.Despawn(val);
            }
            spawnedEnemy.Clear();

            foreach (var val in spawnedWall)
            {
                wallPool.Despawn(val);
            }
            spawnedWall.Clear();
        }

        private void SpawnNextLevel()
        {
            Sequence seq = DOTween.Sequence();
            seq.SetDelay(2f).OnComplete(SpawnLevelAfterDelay);
        }

        private void RestartLevel()
        {
            Sequence seq = DOTween.Sequence();
            seq.SetDelay(2f).OnComplete(RestartLevelAfterDelay);
        }

        private void SpawnLevelAfterDelay()
        {
            DespawnLevel();
            signalSystemFacade.FireSignal<OnNextLevelSignal>();
        }
        
        private void RestartLevelAfterDelay()
        {
            DespawnLevel();
            signalSystemFacade.FireSignal<OnRestartLevelSignal>();
        }

        private void SetDifficulty(OnDifficultyClickedSignal signalData)
        {
            difficulty = signalData.isHardEnabled ? Difficulty.Hard : Difficulty.Easy;
        }
    }
}
