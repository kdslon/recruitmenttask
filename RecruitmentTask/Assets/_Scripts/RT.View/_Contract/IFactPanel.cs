﻿using System;
using RT.Utilities;

namespace RT.View
{
    public interface IFactPanel : IBaseView
    {
        void ShowFactPanel(Level level, Action onComplete);
        void HideFactPanel(Action onComplete);
    }
}
