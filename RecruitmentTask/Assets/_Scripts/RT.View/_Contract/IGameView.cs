﻿namespace RT.View
{
    public interface IGameView : IBaseView
    {
        void ShowTutorial();
        void HideTutorial();
        void EnableRestartButton();
    }
}
