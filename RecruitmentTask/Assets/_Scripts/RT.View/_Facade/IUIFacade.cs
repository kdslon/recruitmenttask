﻿using System;
using RT.Utilities;

namespace RT.View
{
    public interface IUIFacade
    {
        void HideMainMenuView();
        void HideFactPanel(Action onComplete);
        void ShowFactPanel(Level level, Action onComplete);
        void ShowGameView();
        void ShowTutorial();
        void HideTutorial();
        void EnableRestartButton();
    }

}