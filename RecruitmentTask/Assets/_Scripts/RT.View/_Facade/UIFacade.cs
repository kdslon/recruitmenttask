﻿using System;
using RT.Signal;
using RT.Utilities;

namespace RT.View
{
    public class UIFacade : IUIFacade
    {
        private readonly ISignalSystemFacade signalSystemFacade;
        private readonly IMainMenuView mainMenuView;
        private readonly IFactPanel factPanel;
        private readonly IGameView gameView;

        public UIFacade(IMainMenuView mainMenuView, IFactPanel factPanel, ISignalSystemFacade signalSystemFacade, IGameView gameView)
        {
            this.mainMenuView = mainMenuView;
            this.factPanel = factPanel;
            this.signalSystemFacade = signalSystemFacade;
            this.gameView = gameView;
        }
        

        public void HideMainMenuView()
        {
            mainMenuView?.HideView();
        }

        public void ShowFactPanel(Level level, Action onComplete)
        {
            factPanel.ShowFactPanel(level, onComplete);
        }

        public void ShowGameView()
        {
            gameView.ShowView();
        }

        public void ShowTutorial()
        {
            gameView.ShowTutorial();
        }

        public void HideTutorial()
        {
            gameView.HideTutorial();
        }

        public void HideFactPanel(Action onComplete)
        {
            factPanel.HideFactPanel(onComplete);
        }

        public void EnableRestartButton()
        {
            gameView.EnableRestartButton();
        }
    }
}
