﻿using DG.Tweening;
using RT.Signal;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace RT.View
{
    public class GameView : BaseView, IGameView
    {
        [Inject]
        private readonly ISignalSystemFacade signalSystemFacade;
        
        [SerializeField] 
        private Button musicButton;
        
        [SerializeField] 
        private Sprite musicOn;

        [SerializeField] 
        private Sprite musicOff;

        [SerializeField] 
        private Button restartLevel;

        [SerializeField] 
        private RectTransform tutorial;

        private bool isMuteEnabled;
        private readonly Vector3 one = new Vector3(1f, 1f, 1f);

        private const float END_SCALE = 1.2f;
        private const float ANIMATION_DURATION = .1f;
        
        private void Start()
        {
            restartLevel.onClick.AddListener(OnRestartButtonTapped);
            musicButton.onClick.AddListener(OnMusicButtonTapped);
        }

        public void ShowTutorial()
        {
            if(tutorial!=null)
                tutorial.gameObject.SetActive(true);
        }

        public void HideTutorial()
        {
            if(tutorial!=null)
                tutorial.gameObject.SetActive(false);
        }

        public void EnableRestartButton()
        {
            restartLevel.interactable = true;
        }

        private void OnMusicButtonTapped()
        {
            var sequence = DOTween.Sequence();
            sequence
                .Append(musicButton.transform.DOScale(one * END_SCALE, ANIMATION_DURATION))
                .Append(musicButton.transform.DOScale(one, ANIMATION_DURATION));

            isMuteEnabled = !isMuteEnabled;
            musicButton.image.sprite = isMuteEnabled ? musicOff : musicOn;
            
            signalSystemFacade.FireSignal<OnSwitchClickedSignal>();
            signalSystemFacade.FireSignal(new OnMuteClickedSignal(isMuteEnabled));
        }
        
        private void OnRestartButtonTapped()
        {
            var sequence = DOTween.Sequence();
            sequence
                .Append(restartLevel.transform.DOScale(one * END_SCALE, ANIMATION_DURATION))
                .Append(restartLevel.transform.DOScale(one, ANIMATION_DURATION));

            restartLevel.interactable = false;
            signalSystemFacade.FireSignal<OnButtonClickedSignal>();
            signalSystemFacade.FireSignal(new OnLoseSignal());
        }
    }

}