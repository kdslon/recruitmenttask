﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using RT.Utilities;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace RT.View
{
    public class FactPanel : BaseView, IFactPanel
    {
        [SerializeField] 
        private RectTransform rect;
        
        [SerializeField] 
        private TextMeshProUGUI levelTitle;

        [SerializeField] 
        private Image factImage;

        [SerializeField] 
        private TextMeshProUGUI factDescription;

        [SerializeField]
        private List<FunFact> funFacts;

        private const float TIME_TO_READ = 2f;
        private const float ANIMATION_TIME = .5f;
        
        public void ShowFactPanel(Level level, Action onComplete)
        {
            levelTitle.text = level.ToString().ToUpper();
            var fact = funFacts[(int) level];
            factImage.sprite = fact.factSprite;
            factDescription.text = fact.factDescription;

            rect.transform.localPosition = Vector3.down * Screen.height;
            ShowView();
            rect.DOLocalMove(Vector3.zero, .5f).OnComplete(onComplete.Invoke);
        }

        public void HideFactPanel(Action onComplete)
        {
            var seq = DOTween.Sequence();
            seq.SetDelay(TIME_TO_READ)
                .Append(rect.DOLocalMove(Vector3.down * Screen.height, ANIMATION_TIME))
                .OnComplete
                (
                    () =>
                    {
                        onComplete?.Invoke();
                        HideView();
                    }
                );
        }
    }
}