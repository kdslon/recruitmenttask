﻿using DG.Tweening;
using RT.Signal;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Zenject;

namespace RT.View
{
    public class MainMenuView : BaseView, IMainMenuView
    {
        [Inject] 
        private readonly ISignalSystemFacade signalSystemFacade;
        
        [SerializeField] 
        private Button startButton;

        [FormerlySerializedAs("easyButton")] [SerializeField] 
        private Button difficultyButton;

        [SerializeField] 
        private Button exitButton;

        [SerializeField] 
        private Button musicButton;

        [SerializeField] 
        private Sprite musicOn;

        [SerializeField] 
        private Sprite musicOff;

        [SerializeField] 
        private Button gameLogo;

        [SerializeField] 
        private Sprite[] gameLogos;

        [SerializeField] 
        private Button gameName;

        [SerializeField] 
        private TextMeshProUGUI gameNameText;

        [SerializeField] 
        private TextMeshProUGUI difficultyButtonText;

        private bool isMuteEnabled;
        private readonly Vector3 one = new Vector3(1f,1f,1f);
        private readonly string[] gameNames = {"Reflect\nShootout", "Recruitment\nTask"};
        private int namesCounter = 1;
        private bool isHardEnabled = true;

        private const float END_SCALE = 1.2f;
        private const float ANIMATION_DURATION = .1f;
        
        private void Start()
        {
            startButton.onClick.AddListener(OnStartButtonTapped);
            difficultyButton.onClick.AddListener(OnDifficultyButtonTapped);
            exitButton.onClick.AddListener(OnExitButtonTapped);
            musicButton.onClick.AddListener(OnMusicButtonTapped);
            gameLogo.onClick.AddListener(OnGameLogoTapped);
            gameName.onClick.AddListener(OnGameNameTapped);
        }

        private void OnStartButtonTapped()
        {
            var sequence = DOTween.Sequence();
            sequence
                .Append(startButton.transform.DOScale(one * END_SCALE, ANIMATION_DURATION))
                .Append(startButton.transform.DOScale(one, ANIMATION_DURATION));
            
            signalSystemFacade.FireSignal<OnButtonClickedSignal>();
            signalSystemFacade.FireSignal<OnStartGameSignal>();
            
        }

        private void OnDifficultyButtonTapped()
        {
            var sequence = DOTween.Sequence();
            sequence
                .Append(difficultyButton.transform.DOScale(one * END_SCALE, ANIMATION_DURATION))
                .Append(difficultyButton.transform.DOScale(one, ANIMATION_DURATION));

            isHardEnabled = !isHardEnabled;
            difficultyButtonText.text = isHardEnabled ? "HARD" : "EASY";
            
            signalSystemFacade.FireSignal(new OnDifficultyClickedSignal(isHardEnabled));
            signalSystemFacade.FireSignal<OnSwitchClickedSignal>();
        }

        private void OnExitButtonTapped()
        {
            var sequence = DOTween.Sequence();
            sequence
                .Append(exitButton.transform.DOScale(one * END_SCALE, ANIMATION_DURATION))
                .Append(exitButton.transform.DOScale(one, ANIMATION_DURATION));
            
            signalSystemFacade.FireSignal<OnButtonClickedSignal>();
            Application.Quit();
        }

        private void OnMusicButtonTapped()
        {
            var sequence = DOTween.Sequence();
            sequence
                .Append(musicButton.transform.DOScale(one * END_SCALE, ANIMATION_DURATION))
                .Append(musicButton.transform.DOScale(one, ANIMATION_DURATION));

            isMuteEnabled = !isMuteEnabled;
            musicButton.image.sprite = isMuteEnabled ? musicOff : musicOn;
            
            signalSystemFacade.FireSignal<OnSwitchClickedSignal>();
            signalSystemFacade.FireSignal(new OnMuteClickedSignal(isMuteEnabled));
        }

        private void OnGameLogoTapped()
        {
            var sequence = DOTween.Sequence();
            sequence
                .Append(gameLogo.transform.DOScale(one * END_SCALE, ANIMATION_DURATION))
                .Append(gameLogo.transform.DOScale(one, ANIMATION_DURATION));
            var number = Random.Range(0, gameLogos.Length);
            gameLogo.image.sprite = gameLogos[number];
            
            signalSystemFacade.FireSignal<OnSwitchClickedSignal>();
        }

        private void OnGameNameTapped()
        {
            var sequence = DOTween.Sequence();
            sequence
                .Append(gameName.transform.DOScale(one * END_SCALE, ANIMATION_DURATION))
                .Append(gameName.transform.DOScale(one, ANIMATION_DURATION));

            gameNameText.text = gameNames[namesCounter++];
            if (namesCounter == gameNames.Length)
                namesCounter = 0;
            
            signalSystemFacade.FireSignal<OnSwitchClickedSignal>();
        }
    }
}