﻿namespace RT.View
{
    public interface IBaseView
    {
        void ShowView();
        void HideView();
    }
}