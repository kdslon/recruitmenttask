﻿using UnityEngine;

namespace RT.View
{
    [CreateAssetMenu(menuName = "FunFact/NewFact")]
    public class FunFact : ScriptableObject
    {
        public string factDescription;
        public Sprite factSprite;
    }
}