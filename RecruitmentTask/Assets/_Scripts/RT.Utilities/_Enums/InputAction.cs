﻿namespace RT.Input
{
    public enum InputAction
    {
        Started,
        Performed,
        Ended,
        Canceled
    }
}
