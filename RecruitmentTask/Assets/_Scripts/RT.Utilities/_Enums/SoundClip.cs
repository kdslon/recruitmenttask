﻿namespace RT.Utilities
{
    public enum SoundClip
    {
        ClickButton,
        ClickSwitch,
        Laser,
    }
}