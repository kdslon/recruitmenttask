﻿namespace RT.Utilities
{
    public enum BindingMode
    {
        Concrete,
        Mock
    }
}