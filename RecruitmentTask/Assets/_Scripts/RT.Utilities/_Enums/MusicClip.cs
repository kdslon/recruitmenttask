﻿namespace RT.Utilities
{
    public enum MusicClip
    {
        MainMenu,
        NormalLevels,
        AlternateLevelMusic,
        Boss
    }
}
