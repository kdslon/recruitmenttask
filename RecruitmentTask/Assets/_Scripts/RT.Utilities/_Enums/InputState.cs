﻿namespace RT.Utilities
{
    public enum InputState
    {
        Idle, 
        Waiting, 
        Ready, 
        Stopped, 
        End, 
        Abort
    }
}
