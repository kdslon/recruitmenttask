﻿using UnityEngine;

namespace RT.Utilities
{
    public static class Utils
    {
        public static Vector2 CalculateNormalizedPosition(Vector2 position)
        {
            return new Vector2(position.x / Screen.width, position.y / Screen.height);
        }
    }
}