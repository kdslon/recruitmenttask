﻿using RT.States;
using Zenject;

namespace RT.State
{
    public class LevelFourStateFactory : StateFactory<LevelFourState>
    {
        public LevelFourStateFactory(DiContainer container) : base(container){ }

        public override CompositeComponent Create()
        {
            state = container.Instantiate<LevelFourState>();
            state.AddComponent(container.Instantiate<InputComponent>());

            return state;
        }
    }
}
