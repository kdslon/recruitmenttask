﻿using Zenject;

namespace RT.States
{
    public class LevelOneStateFactory : StateFactory<LevelOneState>
    {
        public LevelOneStateFactory(DiContainer container) : base(container) { }

        public override CompositeComponent Create()
        {
            state = container.Instantiate<LevelOneState>();
            state.AddComponent(container.Instantiate<InputComponent>());

            return state;
        }
    }
}