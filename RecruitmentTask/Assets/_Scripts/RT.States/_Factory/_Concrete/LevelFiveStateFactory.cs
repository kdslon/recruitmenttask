﻿using Zenject;

namespace RT.States
{
    public class LevelFiveStateFactory : StateFactory<LevelFiveState>
    {
        public LevelFiveStateFactory(DiContainer container) : base(container) { }

        public override CompositeComponent Create()
        {
            state = container.Instantiate<LevelFiveState>();
            state.AddComponent(container.Instantiate<InputComponent>());

            return state;
        }
    }
}
