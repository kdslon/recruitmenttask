﻿using RT.Installer;
using Zenject;

namespace RT.States
{
    public class LevelThreeStateFactory : StateFactory<LevelThreeState>
    {
        public LevelThreeStateFactory(DiContainer container) : base(container) { }

        public override CompositeComponent Create()
        {
            state = container.Instantiate<LevelThreeState>();
            state.AddComponent(container.Instantiate<InputComponent>());

            return state;
        }
    }
}