﻿using RT.Installer;
using Zenject;

namespace RT.States
{
    public class LevelTwoStateFactory : StateFactory<LevelTwoState>
    {
        public LevelTwoStateFactory(DiContainer container) : base(container) { }

        public override CompositeComponent Create()
        {
            state = container.Instantiate<LevelTwoState>();
            state.AddComponent(container.Instantiate<InputComponent>());

            return state;
        }
    }
}