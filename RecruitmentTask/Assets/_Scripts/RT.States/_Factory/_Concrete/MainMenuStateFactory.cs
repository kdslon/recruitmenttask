﻿using Zenject;

namespace RT.States
{
    public class MainMenuStateFactory : StateFactory<MainMenuState>
    {
        public MainMenuStateFactory(DiContainer container) : base(container) { }

        public override CompositeComponent Create()
        {
            state = container.Instantiate<MainMenuState>();
            state.AddComponent(container.Instantiate<BezierComponent>());
            return state;
        }
    }
}
