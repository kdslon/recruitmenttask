﻿using Zenject;

namespace RT.States
{
    public abstract class StateFactory<T> : IFactory<CompositeComponent> where T : CompositeComponent
    {
        protected readonly DiContainer container;
        protected CompositeComponent state;

        protected StateFactory(DiContainer container)
        {
            this.container = container;
        }

        public abstract CompositeComponent Create();
    }
}
