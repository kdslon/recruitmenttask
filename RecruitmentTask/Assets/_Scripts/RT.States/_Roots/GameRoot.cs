﻿using Zenject;

namespace RT.States
{
    public class GameRoot : Root<MainMenuStateFactory>
    {
        public GameRoot(DiContainer container) : base(container){ }
    }
}
