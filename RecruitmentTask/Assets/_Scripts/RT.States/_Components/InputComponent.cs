﻿using RT.Input;

namespace RT.States
{
    public class InputComponent : PrimitiveComponent
    {
        private readonly IInputSystemFacade inputSystemFacade;

        public InputComponent(IInputSystemFacade inputSystemFacade)
        {
            this.inputSystemFacade = inputSystemFacade;
        }

        public override void Tick()
        {
            inputSystemFacade.UpdateInput();
        }
    }
}
