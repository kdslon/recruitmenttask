﻿using RT.Cam;

namespace RT.States
{
    public class BezierComponent : PrimitiveComponent
    {
        private readonly IBezierCameraMovementSystemFacade bezierCameraMovementSystemFacade;

        public BezierComponent(IBezierCameraMovementSystemFacade bezierCameraMovementSystemFacade)
        {
            this.bezierCameraMovementSystemFacade = bezierCameraMovementSystemFacade;
        }

        public override void Initialize()
        {    
            bezierCameraMovementSystemFacade.InitializeSystem();
        }

        public override void Tick()
        {
            bezierCameraMovementSystemFacade.UpdateCameraPosition();
        }
    }
}
