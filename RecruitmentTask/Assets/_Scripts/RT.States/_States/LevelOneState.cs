﻿using RT.GameLogic;
using RT.Music;
using RT.Signal;
using RT.Utilities;
using RT.View;

namespace RT.States
{
    public class LevelOneState : CompositeComponent
    {
        private readonly IUIFacade uiFacade;
        private readonly IMusicSystemFacade musicSystemFacade;
        private readonly ILevelControllerFacade levelControllerFacade;
        private readonly ISceneRootFacade sceneRootFacade;
        private readonly ISignalSystemFacade signalSystemFacade;
        
        public LevelOneState(IUIFacade uiFacade, IMusicSystemFacade musicSystemFacade, ISignalSystemFacade signalSystemFacade, ILevelControllerFacade levelControllerFacade, ISceneRootFacade sceneRootFacade)
        {
            this.uiFacade = uiFacade;
            this.musicSystemFacade = musicSystemFacade;
            this.levelControllerFacade = levelControllerFacade;
            this.signalSystemFacade = signalSystemFacade;
            this.sceneRootFacade = sceneRootFacade;
        }

        public override void Initialize()
        {
            signalSystemFacade.FireSignal(new OnGenerateLevelSignal(Level.Level1));
            uiFacade.HideFactPanel(() => musicSystemFacade.PlayMusic(MusicClip.NormalLevels));
            signalSystemFacade.SubscribeSignal<OnNextLevelSignal>(ActivateNextLevel);
            signalSystemFacade.SubscribeSignal<OnRestartLevelSignal>(RestartLevel);
            base.Initialize();
            uiFacade.ShowTutorial();
            levelControllerFacade.InitializeController();
            uiFacade.EnableRestartButton();
        }

        public override void Dispose()
        {
            uiFacade.HideTutorial();
            levelControllerFacade.DisposeController();
            signalSystemFacade.UnsubscribeSignal<OnNextLevelSignal>(ActivateNextLevel);
            signalSystemFacade.UnsubscribeSignal<OnRestartLevelSignal>(RestartLevel);
            base.Dispose();
        }

        private void ActivateNextLevel()
        {
            uiFacade.ShowFactPanel(Level.Level2, () => sceneRootFacade.CreateNewState<LevelTwoStateFactory>());
        }

        private void RestartLevel()
        {
            uiFacade.ShowFactPanel(Level.Level1, () => sceneRootFacade.CreateNewState<LevelOneStateFactory>());
        }
    }
}
