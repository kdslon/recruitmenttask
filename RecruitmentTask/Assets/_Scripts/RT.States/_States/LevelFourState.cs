﻿using System.Collections;
using System.Collections.Generic;
using RT.GameLogic;
using RT.Signal;
using RT.State;
using RT.Utilities;
using RT.View;
using UnityEngine;

namespace RT.States
{
    public class LevelFourState : CompositeComponent
    {
        private readonly IUIFacade uiFacade;
        private readonly ILevelControllerFacade levelControllerFacade;
        private readonly ISceneRootFacade sceneRootFacade;
        private readonly ISignalSystemFacade signalSystemFacade;

        public LevelFourState(IUIFacade uiFacade, ILevelControllerFacade levelControllerFacade, ISceneRootFacade sceneRootFacade, ISignalSystemFacade signalSystemFacade)
        {
            this.uiFacade = uiFacade;
            this.levelControllerFacade = levelControllerFacade;
            this.sceneRootFacade = sceneRootFacade;
            this.signalSystemFacade = signalSystemFacade;
        }

        public override void Initialize()
        {
            signalSystemFacade.FireSignal(new OnGenerateLevelSignal(Level.Level4));
            uiFacade.HideFactPanel(null);
            signalSystemFacade.SubscribeSignal<OnNextLevelSignal>(ActivateNextLevel);
            signalSystemFacade.SubscribeSignal<OnRestartLevelSignal>(RestartLevel);
            base.Initialize();
            levelControllerFacade.InitializeController();
            uiFacade.EnableRestartButton();
        }

        public override void Dispose()
        {
            base.Dispose();
            levelControllerFacade.DisposeController();
            signalSystemFacade.UnsubscribeSignal<OnNextLevelSignal>(ActivateNextLevel);
            signalSystemFacade.UnsubscribeSignal<OnRestartLevelSignal>(RestartLevel);
        }

        private void ActivateNextLevel()
        {
            uiFacade.ShowFactPanel(Level.Boss, () => sceneRootFacade.CreateNewState<LevelFiveStateFactory>());
        }
        
        private void RestartLevel()
        {
            uiFacade.ShowFactPanel(Level.Level4, () => sceneRootFacade.CreateNewState<LevelFourStateFactory>());
        }
    }
}
