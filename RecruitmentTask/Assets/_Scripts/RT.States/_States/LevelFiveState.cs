﻿using RT.GameLogic;
using RT.Music;
using RT.Signal;
using RT.Utilities;
using RT.View;

namespace RT.States
{
    public class LevelFiveState : CompositeComponent
    {
        private readonly IUIFacade uiFacade;
        private readonly ILevelControllerFacade levelControllerFacade;
        private readonly ISceneRootFacade sceneRootFacade;
        private readonly ISignalSystemFacade signalSystemFacade;
        private readonly IMusicSystemFacade musicSystemFacade;
        
        public LevelFiveState(IUIFacade uiFacade, ILevelControllerFacade levelControllerFacade, ISceneRootFacade sceneRootFacade, ISignalSystemFacade signalSystemFacade, IMusicSystemFacade musicSystemFacade)
        {
            this.uiFacade = uiFacade;
            this.levelControllerFacade = levelControllerFacade;
            this.sceneRootFacade = sceneRootFacade;
            this.signalSystemFacade = signalSystemFacade;
            this.musicSystemFacade = musicSystemFacade;
        }

        public override void Initialize()
        {
            musicSystemFacade.PlayMusic(MusicClip.Boss);
            signalSystemFacade.FireSignal(new OnGenerateLevelSignal(Level.Boss));
            uiFacade.HideFactPanel(null);
            signalSystemFacade.SubscribeSignal<OnNextLevelSignal>(ActivateNextLevel);
            signalSystemFacade.SubscribeSignal<OnRestartLevelSignal>(RestartLevel);
            base.Initialize();
            levelControllerFacade.InitializeController();
            uiFacade.EnableRestartButton();
        }

        public override void Dispose()
        {
            base.Dispose();
            levelControllerFacade.DisposeController();
            signalSystemFacade.UnsubscribeSignal<OnNextLevelSignal>(ActivateNextLevel);
            signalSystemFacade.UnsubscribeSignal<OnRestartLevelSignal>(RestartLevel);
        }

        private void ActivateNextLevel()
        {
            uiFacade.ShowFactPanel(Level.Level1, () => sceneRootFacade.CreateNewState<LevelOneStateFactory>());
        }
        
        private void RestartLevel()
        {
            uiFacade.ShowFactPanel(Level.Boss, () => sceneRootFacade.CreateNewState<LevelFiveStateFactory>());
        }
    }
}
