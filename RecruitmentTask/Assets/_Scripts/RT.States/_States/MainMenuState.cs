﻿using RT.Music;
using RT.Signal;
using RT.Utilities;
using RT.View;

namespace RT.States
{
    public class MainMenuState : CompositeComponent
    {
        private readonly ISignalSystemFacade signalSystemFacade;
        private readonly IMusicSystemFacade musicSystemFacade;
        private readonly IUIFacade uiFacade;
        private readonly ISceneRootFacade sceneRootFacade;

        public MainMenuState(ISignalSystemFacade signalSystemFacade, IMusicSystemFacade musicSystemFacade, IUIFacade uiFacade, ISceneRootFacade sceneRootFacade)
        {
            this.signalSystemFacade = signalSystemFacade;
            this.musicSystemFacade = musicSystemFacade;
            this.uiFacade = uiFacade;
            this.sceneRootFacade = sceneRootFacade;
        }

        public override void Initialize()
        {
            musicSystemFacade.PlayMusic(MusicClip.MainMenu);
            signalSystemFacade.SubscribeSignal<OnStartGameSignal>(() => uiFacade.ShowFactPanel(Level.Level1, ()=> sceneRootFacade.CreateNewState<LevelOneStateFactory>()));
        }

        public override void Dispose()
        {
            uiFacade.HideMainMenuView();
            uiFacade.ShowGameView();
            base.Dispose();
        }
    }
}
