﻿using RT.GameLogic;
using RT.Signal;
using RT.States;
using RT.Utilities;
using RT.View;

namespace RT.Installer
{
    public class LevelTwoState : CompositeComponent
    {
        private readonly IUIFacade uiFacade;
        private readonly ILevelControllerFacade levelControllerFacade;
        private readonly ISceneRootFacade sceneRootFacade;
        private readonly ISignalSystemFacade signalSystemFacade;

        public LevelTwoState(IUIFacade uiFacade, ILevelControllerFacade levelControllerFacade, ISceneRootFacade sceneRootFacade, ISignalSystemFacade signalSystemFacade)
        {
            this.uiFacade = uiFacade;
            this.levelControllerFacade = levelControllerFacade;
            this.sceneRootFacade = sceneRootFacade;
            this.signalSystemFacade = signalSystemFacade;
        }

        public override void Initialize()
        {
            signalSystemFacade.FireSignal(new OnGenerateLevelSignal(Level.Level2));
            uiFacade.HideFactPanel(null);
            signalSystemFacade.SubscribeSignal<OnNextLevelSignal>(ActivateNextLevel);
            signalSystemFacade.SubscribeSignal<OnRestartLevelSignal>(RestartLevel);
            base.Initialize();
            levelControllerFacade.InitializeController();
            uiFacade.EnableRestartButton();
        }
        
        public override void Dispose()
        {
            base.Dispose();
            levelControllerFacade.DisposeController();
            signalSystemFacade.UnsubscribeSignal<OnNextLevelSignal>(ActivateNextLevel);
            signalSystemFacade.UnsubscribeSignal<OnRestartLevelSignal>(RestartLevel);
        }

        private void ActivateNextLevel()
        {
            uiFacade.ShowFactPanel(Level.Level3, () => sceneRootFacade.CreateNewState<LevelThreeStateFactory>());
        }
        
        private void RestartLevel()
        {
            uiFacade.ShowFactPanel(Level.Level2, () => sceneRootFacade.CreateNewState<LevelTwoStateFactory>());
        }
    }
}
