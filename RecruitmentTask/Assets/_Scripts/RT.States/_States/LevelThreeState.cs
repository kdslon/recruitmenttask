﻿using System.Collections;
using System.Collections.Generic;
using RT.GameLogic;
using RT.Music;
using RT.Signal;
using RT.State;
using RT.States;
using RT.Utilities;
using RT.View;
using UnityEngine;

namespace RT.Installer
{
    public class LevelThreeState : CompositeComponent
    {
        private readonly IUIFacade uiFacade;
        private readonly ILevelControllerFacade levelControllerFacade;
        private readonly ISceneRootFacade sceneRootFacade;
        private readonly ISignalSystemFacade signalSystemFacade;
        private readonly IMusicSystemFacade musicSystemFacade;
        
        public LevelThreeState(IUIFacade uiFacade, ILevelControllerFacade levelControllerFacade, ISceneRootFacade sceneRootFacade, ISignalSystemFacade signalSystemFacade, IMusicSystemFacade musicSystemFacade)
        {
            this.uiFacade = uiFacade;
            this.levelControllerFacade = levelControllerFacade;
            this.sceneRootFacade = sceneRootFacade;
            this.signalSystemFacade = signalSystemFacade;
            this.musicSystemFacade = musicSystemFacade;
        }

        public override void Initialize()
        {
            musicSystemFacade.PlayMusic(MusicClip.AlternateLevelMusic);
            signalSystemFacade.FireSignal(new OnGenerateLevelSignal(Level.Level3));
            uiFacade.HideFactPanel(null);
            signalSystemFacade.SubscribeSignal<OnNextLevelSignal>(ActivateNextLevel);
            signalSystemFacade.SubscribeSignal<OnRestartLevelSignal>(RestartLevel);
            base.Initialize();
            levelControllerFacade.InitializeController();
            uiFacade.EnableRestartButton();
        }
        
        public override void Dispose()
        {
            base.Dispose();
            levelControllerFacade.DisposeController();
            signalSystemFacade.UnsubscribeSignal<OnNextLevelSignal>(ActivateNextLevel);
            signalSystemFacade.UnsubscribeSignal<OnRestartLevelSignal>(RestartLevel);
        }

        private void ActivateNextLevel()
        {
            uiFacade.ShowFactPanel(Level.Level4, () => sceneRootFacade.CreateNewState<LevelFourStateFactory>());
        }
        
        private void RestartLevel()
        {
            uiFacade.ShowFactPanel(Level.Level3, () => sceneRootFacade.CreateNewState<LevelThreeStateFactory>());
        }
    }
}
