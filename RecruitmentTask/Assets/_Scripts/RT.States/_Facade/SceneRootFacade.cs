﻿using Zenject;

namespace RT.States
{
    public class SceneRootFacade : ISceneRootFacade
    {
        private readonly IRoot root;

        public SceneRootFacade(IRoot root)
        {
            this.root = root;
        }
        
        public void CreateNewState<T>() where T : IFactory<CompositeComponent>
        {
            root.CreateNewState<T>();
        }

        public void EnableComponent<T>() where T : IComponent
        {
            root.EnableComponent<T>();
        }

        public void DisableComponent<T>() where T : IComponent
        {
            root.DisableComponent<T>();
        }
    }
}
