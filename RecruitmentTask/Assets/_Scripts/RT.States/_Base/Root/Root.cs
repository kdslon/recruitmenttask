﻿using System;
using Zenject;

namespace RT.States
{
    public class Root<TStartState> : IRoot, IInitializable, ITickable, IDisposable
        where TStartState : IFactory<CompositeComponent>
    {
        private readonly DiContainer container;
        private CompositeComponent currentlyActiveState;

        protected Root(DiContainer container)
        {
            this.container = container;
        }

        public void Initialize()
        {
            CreateNewState<TStartState>();
        }

        public void Tick()
        {
            currentlyActiveState?.Tick();
        }

        public void Dispose()
        {
            CompleteCurrentState();
        }

        public void CreateNewState<T>() where T : IFactory<CompositeComponent>
        {
            CompleteCurrentState();
            var factory = container.Instantiate<T>();
            CreateNewState(factory);
        }
        
        private void CreateNewState(IFactory<CompositeComponent> stateFactory)
        {
            currentlyActiveState = stateFactory.Create();
            currentlyActiveState?.Initialize();
        }

        public void EnableComponent<T>() where T : IComponent
        {
            currentlyActiveState.Enable<T>();
        }

        public void DisableComponent<T>() where T : IComponent
        {
            currentlyActiveState.Disable<T>();
        }
        
        private void CompleteCurrentState()
        {
            currentlyActiveState?.Dispose();
            currentlyActiveState = null;
        }
    }
}
