﻿using Zenject;

namespace RT.States
{
    public interface IRoot
    {
        void CreateNewState<T>() where T : IFactory<CompositeComponent>;
        void EnableComponent<T>() where T : IComponent;
        void DisableComponent<T>() where T : IComponent;
    }
}
