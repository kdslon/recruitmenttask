﻿namespace RT.States
{
    public class PrimitiveComponent : IComponent
    {
        private bool isActive = true;
        
        public virtual void Initialize(){}
        
        public virtual void Tick(){}
        
        public virtual void Dispose(){}
        
        public virtual void Enable()
        {
            isActive = true;
        }
        
        public virtual void Disable()
        {
            isActive = false;
        }
        
        public bool IsActive()
        {
            return isActive;
        }
    }
}
