﻿namespace RT.States
{
    public interface IComponent
    {
        void Initialize();
        void Tick();
        void Dispose();
        void Enable();
        void Disable();
        bool IsActive();
    }
}