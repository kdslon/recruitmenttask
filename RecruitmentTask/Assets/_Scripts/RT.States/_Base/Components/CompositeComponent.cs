﻿using System.Collections.Generic;

namespace RT.States
{
    public abstract class CompositeComponent : IComponent
    {
        private readonly List<IComponent> components = new List<IComponent>();
        private bool isActive = true;
        
        public virtual void Initialize()
        {
            foreach (var component in components)
            {
                component.Initialize();
            }
        }

        public virtual void Tick()
        {
            foreach (var component in components)
            {
                if(component.IsActive())
                    component.Tick();
            }
        }

        public virtual void Dispose()
        {
            foreach (var component in components)
            {
                component.Dispose();
            }
        }

        public virtual void Enable()
        {
            isActive = true;
        }
        
        public void Enable<T>()
        {
            components.Find(x => x.GetType() == typeof(T)).Enable();
        }

        public virtual void Disable()
        {
            isActive = false;
        }
        
        public void Disable<T>()
        {
            components.Find(x => x.GetType() == typeof(T)).Enable();
        }

        public bool IsActive()
        {
            return isActive;
        }

        public void AddComponent(IComponent component)
        {
            components.Add(component);
        }
    }
}
