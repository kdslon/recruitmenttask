﻿using RT.Cam;

public class BezierCameraMovementSystemFacade : IBezierCameraMovementSystemFacade
{
    private readonly IBezierCameraMovementSystem bezierCameraMovementSystem;

    public BezierCameraMovementSystemFacade(IBezierCameraMovementSystem bezierCameraMovementSystem)
    {
        this.bezierCameraMovementSystem = bezierCameraMovementSystem;
    }

    public void InitializeSystem()
    {
        bezierCameraMovementSystem.InitializeSystem();
    }

    public void UpdateCameraPosition()
    {
        bezierCameraMovementSystem.UpdateCameraPosition();
    }
}
