﻿using RT.Bezier;
using UnityEngine;

namespace RT.Cam
{
    public class BezierCameraMovementSystem : MonoBehaviour, IBezierCameraMovementSystem
    {
        [SerializeField] 
        private BezierCurve bezierCurve;
        
        [SerializeField] 
        private Camera mainCamera;

        [SerializeField] 
        private Transform lookPoint;

        [SerializeField] 
        private float fullLoopTime = 300f;

        private int pointsArrayLength;
        private float startTime;
        private float currentTime;
        private int currentPoint;

        public void InitializeSystem()
        {
            SetLookAt();
            
            currentPoint = 0;
            startTime = Time.time;
        }
        
        private void SetLookAt()
        {
            var points = bezierCurve.GetAnchorPoints();
            foreach (var val in points)
            {
                val.transform.LookAt(lookPoint);
            }
        }

        public void UpdateCameraPosition()
        {
            currentTime = (Time.time - startTime) / fullLoopTime;
            if (currentTime > 1f)
            {
                startTime = Time.time;
                currentTime = 0f;
            }

            mainCamera.transform.position = bezierCurve.GetPointAt(currentTime);
            mainCamera.transform.LookAt(lookPoint);
        }
    }
}
