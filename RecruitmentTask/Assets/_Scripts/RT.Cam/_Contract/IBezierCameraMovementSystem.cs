﻿
namespace RT.Cam
{
    public interface IBezierCameraMovementSystem
    {
        void InitializeSystem();
        void UpdateCameraPosition();
    }
}
