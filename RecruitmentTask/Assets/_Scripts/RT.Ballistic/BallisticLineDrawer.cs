﻿using RT.Fire;
using RT.Input;
using UnityEngine;
using UnityEngine.Serialization;

namespace RT.Ballistic
{
    [RequireComponent(typeof(LineRenderer))]
    public class BallisticLineDrawer : MonoBehaviour
    {
        [FormerlySerializedAs("gunPosition")] [SerializeField]
        private Transform firePosition;
        
        [SerializeField]
        private LineRenderer ballisticLine;
        
        [SerializeField]
        private BaseCharacterFightSystem characterFightSystem;
        
        [SerializeField]
        private float maxLineLength = 20;

        [SerializeField]
        private int maxReflections = 3;
        
        private int linePoints = 1;
        private Ray ray;
        private RaycastHit raycastHit;
        private Vector3 lineDirection;
        private BaseCharacterFightSystem targetFightSystem;

        private const string REFLECTABLE_TAG = "Reflectable";
        
        public BaseCharacterFightSystem GetCharacterFightSystem()
        {
            return characterFightSystem;
        }

        public void InitializeSystem(IInputSystemFacade inputSystemFacade)
        {
            inputSystemFacade.AddListener<Drag>(InputAction.Performed, DrawBallisticLine);
            characterFightSystem?.InitializeSystem(inputSystemFacade);
            ballisticLine.enabled = true;
            DrawBallisticLine();
        }

        public void DisposeSystem(IInputSystemFacade inputSystemFacade)
        {
            inputSystemFacade.RemoveListener<Drag>(InputAction.Performed, DrawBallisticLine);
            ballisticLine.enabled = false;
            characterFightSystem?.DisposeSystem(inputSystemFacade);
        }

        public void DrawBallisticLine()
        {
            var position = firePosition.position;
            var leftLength = maxLineLength;
            ray = new Ray(position, firePosition.forward);
            
            ballisticLine.positionCount = linePoints;
            ballisticLine.SetPosition(0, position);
            for (var i = 0; i <= maxReflections; ++i)
            {
                targetFightSystem = null;
                characterFightSystem.UpdateTarget(targetFightSystem);
                if (Physics.Raycast(ray.origin, ray.direction, out raycastHit, leftLength))
                {
                    var positionCount = ballisticLine.positionCount;
                    positionCount++;
                    ballisticLine.positionCount = positionCount;
                    ballisticLine.SetPosition(positionCount - 1, raycastHit.point);
                    
                    leftLength -= Vector3.Distance(ray.origin, raycastHit.point);
                    lineDirection = Vector3.Reflect(ray.direction, raycastHit.normal);
                    
                    ray = new Ray(raycastHit.point, lineDirection);
                    targetFightSystem = raycastHit.collider.gameObject.GetComponent<BaseCharacterFightSystem>();
                    characterFightSystem.UpdateTarget(targetFightSystem);
                    
                    if(!raycastHit.collider.CompareTag(REFLECTABLE_TAG))
                        break;
                }
                else
                {
                    var positionCount = ballisticLine.positionCount;
                    positionCount += 1;
                    ballisticLine.positionCount = positionCount;
                    ballisticLine.SetPosition(positionCount-1, ray.origin + ray.direction * leftLength);
                }
            }
        }
    }
}
