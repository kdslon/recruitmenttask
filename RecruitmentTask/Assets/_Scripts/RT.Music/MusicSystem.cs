﻿using RT.Signal;
using RT.Utilities;
using UnityEngine;
using UnityEngine.Audio;
using Zenject;

namespace RT.Music
{
    public class MusicSystem : MonoBehaviour, IMusicSystem
    {
        [SerializeField] 
        private AudioMixer mixer;
        
        [SerializeField] 
        private AudioSource gameMusic;

        [SerializeField] 
        private AudioClip menuMusic;
        
        [SerializeField] 
        private AudioClip normalLevels;

        [SerializeField] 
        private AudioClip bossIntro;

        [SerializeField] 
        private AudioClip bossLoop;
        
        [SerializeField] 
        private AudioClip alternateLevelMusic;

        [SerializeField] 
        private AudioSource sounds;

        [SerializeField] 
        private AudioClip clickButton;
        
        [SerializeField] 
        private AudioClip clickSwitch;

        [SerializeField] 
        private AudioClip laser;

        [Inject]
        private readonly ISignalSystemFacade signalSystemFacade;

        private const string VOLUME_MIXER_VARIABLE = "Volume";
        private const float MUTE_LEVEL = -80f;
        private const float NORMAL_LEVEL = 0f;
        
        private void Start()
        {
            signalSystemFacade.SubscribeSignal<OnButtonClickedSignal>(()=>PlaySound(SoundClip.ClickButton));
            signalSystemFacade.SubscribeSignal<OnSwitchClickedSignal>(()=>PlaySound(SoundClip.ClickSwitch));
            signalSystemFacade.SubscribeSignal<OnShootSignal>(()=>PlaySound(SoundClip.Laser));
            signalSystemFacade.SubscribeSignal<OnMuteClickedSignal>(SetMute);
        }

        public void PlayMusic(MusicClip clip)
        {
            switch (clip)
            {
                case MusicClip.MainMenu:
                    if (gameMusic.clip == menuMusic)
                        break;
                    gameMusic.clip = menuMusic;
                    gameMusic.Play();
                    break;
                case MusicClip.NormalLevels:
                    if (gameMusic.clip == normalLevels)
                        break;
                    gameMusic.clip = normalLevels;
                    gameMusic.Play();
                    break;
                case MusicClip.AlternateLevelMusic:
                    if (gameMusic.clip == alternateLevelMusic)
                        break;
                    gameMusic.clip = alternateLevelMusic;
                    gameMusic.Play();
                    break;
                case MusicClip.Boss:
                    if (gameMusic.clip == bossLoop)
                        break;
                    gameMusic.clip = bossLoop;
                    gameMusic.PlayOneShot(bossIntro);
                    gameMusic.PlayDelayed(bossIntro.length);
                    break;
                default:
                    Debug.LogError("Wrong clip");
                    break;
            }
        }

        public void PlaySound(SoundClip clip)
        {
            switch (clip)
            {
                case SoundClip.ClickButton:
                    sounds.PlayOneShot(clickButton);
                    break;
                case SoundClip.ClickSwitch:
                    sounds.PlayOneShot(clickSwitch);
                    break;
                case SoundClip.Laser:
                    sounds.PlayOneShot(laser);
                    break;
                default:
                    Debug.LogError("Wrong clip");
                    break;
            }
        }

        public void SetMute(OnMuteClickedSignal signalData)
        {
            mixer.SetFloat(VOLUME_MIXER_VARIABLE, signalData.isMute ? MUTE_LEVEL : NORMAL_LEVEL);
        }
    }
}