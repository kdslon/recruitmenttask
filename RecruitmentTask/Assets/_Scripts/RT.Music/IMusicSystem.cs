﻿using RT.Signal;
using RT.Utilities;

namespace RT.Music
{
    public interface IMusicSystem
    {
        void PlayMusic(MusicClip clip);
        void PlaySound(SoundClip clip);
        void SetMute(OnMuteClickedSignal signalData);
    }
}
