﻿using RT.Signal;
using RT.Utilities;

namespace RT.Music
{
    public class MusicSystemFacade : IMusicSystemFacade
    {
        private readonly IMusicSystem musicSystem;

        public MusicSystemFacade(IMusicSystem musicSystem)
        {
            this.musicSystem = musicSystem;
        }

        public void PlayMusic(MusicClip clip)
        {
            musicSystem.PlayMusic(clip);
        }

        public void PlaySound(SoundClip clip)
        {
            musicSystem.PlaySound(clip);
        }

        public void SetMute(OnMuteClickedSignal signalData)
        {
            musicSystem.SetMute(signalData);
        }
    }
}
