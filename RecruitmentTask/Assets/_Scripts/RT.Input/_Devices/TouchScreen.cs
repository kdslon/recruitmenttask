﻿using RT.Utilities;
using UnityEngine;

namespace RT.Input
{
    public class TouchScreen : IInputDevice
    {
        public bool IsTouchReceived()
        {
            return UnityEngine.Input.touches.Length == 1;
        }

        public Vector2 GetPositionType(VectorType posType)
        {
            var pos = UnityEngine.Input.GetTouch(0).position;
            return posType == VectorType.Normal ? pos : Utils.CalculateNormalizedPosition(pos);
        }
    }
}
