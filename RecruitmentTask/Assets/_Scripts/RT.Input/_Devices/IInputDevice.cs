﻿using RT.Utilities;
using UnityEngine;

namespace RT.Input
{
    public interface IInputDevice
    {
        bool IsTouchReceived();
        Vector2 GetPositionType(VectorType posType);
    }
}
