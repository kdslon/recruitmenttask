﻿using RT.Utilities;
using UnityEngine;

namespace RT.Input
{
    public class Mouse : IInputDevice
    {
        public bool IsTouchReceived()
        {
            return UnityEngine.Input.GetMouseButton(0);
        }

        public Vector2 GetPositionType(VectorType posType)
        {
            var pos = (Vector2) UnityEngine.Input.mousePosition;
            return posType == VectorType.Normal ? pos : Utils.CalculateNormalizedPosition(pos);
        }
    }
}
