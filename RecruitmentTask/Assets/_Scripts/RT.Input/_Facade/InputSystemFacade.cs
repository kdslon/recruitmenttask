﻿using System;

namespace RT.Input
{
    public class InputSystemFacade : IInputSystemFacade
    {
        private readonly IInputSystem inputSystem;

        public InputSystemFacade(IInputSystem inputSystem)
        {
            this.inputSystem = inputSystem;
        }

        public float GetDragSign()
        {
            return inputSystem.GetDragSign();
        }

        public float GetYDragTouchPoint()
        {
            return inputSystem.GetYDragTouchPoint();
        }

        public void EnableInput<TInputType>() where TInputType : InputInteraction
        {
            inputSystem.EnableInput<TInputType>();
        }

        public void DisableInput<TInputType>() where TInputType : InputInteraction
        {
            inputSystem.DisableInput<TInputType>();
        }

        public void AddListener<TInputType>(InputAction phase, Action action) where TInputType : InputInteraction
        {
            inputSystem.AddListener<TInputType>(phase, action);
        }

        public void RemoveListener<TInputType>(InputAction phase, Action action) where TInputType : InputInteraction
        {
            inputSystem.RemoveListener<TInputType>(phase, action);
        }

        public void UpdateInput()
        {
            inputSystem.UpdateInput();
        }
    }
}
