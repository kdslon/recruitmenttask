﻿using System;
using System.Collections.Generic;
using RT.Utilities;

namespace RT.Input
{
    public class InputSystem : IInputSystem
    {
        private readonly InputInteraction[] inputInteractions;
        private readonly Tap tap;
        private readonly Drag drag;

        public InputSystem(List<IInputDevice> devices)
        {
            tap = new Tap(devices);
            drag = new Drag(devices);
            inputInteractions = new InputInteraction[]{tap, drag};

            tap.EnableInput();
            drag.EnableInput();
        }
        
        public void EnableInput<TInputType>() where TInputType : InputInteraction
        {
            Array.Find(inputInteractions, input => input.GetType() == typeof(TInputType)).EnableInput();
        }

        public void DisableInput<TInputType>() where TInputType : InputInteraction
        {
            Array.Find(inputInteractions, input => input.GetType() == typeof(TInputType)).DisableInput();
        }
        
        public void AddListener<TInputType>(InputAction phase, Action action) where TInputType : InputInteraction
        {
            Array.Find(inputInteractions, input => input.GetType() == typeof(TInputType)).AddListener(phase, action);
        }

        public void RemoveListener<TInputType>(InputAction phase, Action action) where TInputType : InputInteraction
        {
            Array.Find(inputInteractions, input => input.GetType() == typeof(TInputType)).RemoveListener(phase, action);
        }
        
        public float GetDragSign()
        {
            return drag.GetDragSign();
        }

        public float GetYDragTouchPoint()
        {
            return drag.GetYTouchPoint();
        }

        public void UpdateInput()
        {
            tap.Process();
            drag.Process();
            PerformInputActions();
        }

        private void PerformInputActions()
        {
            foreach (var input in inputInteractions)
            {
                if (input.GetCurrentState() == InputState.Waiting || input.GetCurrentState() == InputState.Stopped || !input.IsInputEnabled())
                    continue;

                if (input.GetCurrentState() == InputState.Ready)
                {
                    input.InvokeStart();
                    input.InvokePerformed();
                    break;
                }

                if (input.GetCurrentState() == InputState.End)
                {
                    input.InvokeEnded();
                }
                else if (input.GetCurrentState() == InputState.Abort)
                {
                    input.InvokeCanceled();
                }
            }
        }
    }
}
