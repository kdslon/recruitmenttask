﻿using System;

namespace RT.Input
{
    public interface IInputSystem
    {
        float GetDragSign();
        float GetYDragTouchPoint();
        void EnableInput<TInputType>() where TInputType : InputInteraction;
        void DisableInput<TInputType>() where TInputType : InputInteraction;
        void AddListener<TInputType>(InputAction phase, Action action) where TInputType : InputInteraction;
        void RemoveListener<TInputType>(InputAction phase, Action action) where TInputType : InputInteraction;
        void UpdateInput();
    }
}
