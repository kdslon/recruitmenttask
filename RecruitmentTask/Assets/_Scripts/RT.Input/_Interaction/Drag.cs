﻿using System.Collections.Generic;
using RT.Utilities;
using UnityEngine;

namespace RT.Input
{
    public class Drag : InputInteraction
    {
        private const float DEAD_ZONE = 0.0003f;
        private Vector2 previousFrameTouchPos;
        private float dragSign;
        private float yAxisTouchPoint;

        public Drag(List<IInputDevice> devices) : base(devices) { }

        public override void Process()
        {
            if (IsActionEnded)
                return;

            if (isFollowed)
            {
                isStillFollowed = IsTouchReceived();
                if (!isStillFollowed)
                {
                    SetFinalStateAndResetInput();
                    return;
                }

                var currentFrameTouchPos = GetTouchPositionType(VectorType.Normalized);
                dragSign = Mathf.Sign(previousFrameTouchPos.x - currentFrameTouchPos.x);
                yAxisTouchPoint = currentFrameTouchPos.y;
                if (Vector2.Distance(previousFrameTouchPos, currentFrameTouchPos) < DEAD_ZONE)
                {
                    SetStandbyState();
                    return;
                }

                previousFrameTouchPos = currentFrameTouchPos;
                currentState = InputState.Ready;
            }
            else
            {
                isFollowed = IsTouchReceived();
                if (!isFollowed) return;
                previousFrameTouchPos = GetTouchPositionType(VectorType.Normalized);
                currentState = InputState.Waiting;
            }
        }

        public float GetDragSign()
        {
            return dragSign;
        }

        public float GetYTouchPoint()
        {
            return yAxisTouchPoint;
        }
    }
}
