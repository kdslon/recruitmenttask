﻿using System.Collections.Generic;
using RT.Utilities;
using UnityEngine;
using UnityEngine.EventSystems;

namespace RT.Input
{
    public class Tap : InputInteraction
    {
        private float startTouchTime;
        private float currentTouchTime;
        private const float MAX_TOUCH_TIME = .3f;
        
        public Tap(List<IInputDevice> devices) : base(devices) { }

        public override void Process()
        {
            if (IsActionEnded || EventSystem.current.IsPointerOverGameObject())
                return;

            if (currentState == InputState.Ready)
            {
                currentState = InputState.End;
                return;
            }

            if (isFollowed)
            {
                isStillFollowed = IsTouchReceived();
                if (isStillFollowed)
                {
                    currentTouchTime = Time.time - startTouchTime;
                    currentState = currentTouchTime <= MAX_TOUCH_TIME ? InputState.Waiting : InputState.Abort;
                }
                else
                {
                    if(currentTouchTime < MAX_TOUCH_TIME)
                        currentState = InputState.Ready;
                    Reset();
                }
            }
            else
            {
                isFollowed = IsTouchReceived();
                if (!isFollowed) return;

                startTouchTime = Time.time;
                currentState = InputState.Waiting;
            }
        }

        protected override void Reset()
        {
            base.Reset();
            currentTouchTime = 0f;
            startTouchTime = 0f;
        }
    }
}
