﻿using System;
using System.Collections.Generic;
using System.Linq;
using RT.Utilities;
using UnityEngine;

namespace RT.Input
{
    public abstract class InputInteraction
    {
        protected InputState currentState = InputState.Idle;
        protected bool isFollowed;
        protected bool isStillFollowed;

        private readonly List<IInputDevice> devices;
        private Action started;
        private Action performed;
        private Action canceled;
        private Action ended;

        private bool isAlreadyStarted;
        private bool isEnabled;

        protected bool IsActionEnded => currentState == InputState.Abort || currentState == InputState.End;

        protected InputInteraction(List<IInputDevice> devices)
        {
            this.devices = devices;
            isEnabled = false;
        }

        public abstract void Process();

        protected virtual void Reset()
        {
            isFollowed = false;
            isStillFollowed = false;
            isAlreadyStarted = false;
        }
        
        public void EnableInput()
        {
            isEnabled = true;
        }

        public void DisableInput()
        {
            isEnabled = false;
        }

        public bool IsInputEnabled()
        {
            return isEnabled;
        }
        
        public InputState GetCurrentState()
        {
            return currentState;
        }
        
        protected void SetStandbyState()
        {
            if (currentState == InputState.Ready || currentState == InputState.Stopped)
                currentState = InputState.Stopped;
            else
                currentState = InputState.Waiting;
        }

        protected void SetFinalStateAndResetInput()
        {
            if (currentState == InputState.Ready || currentState == InputState.Stopped)
                currentState = InputState.End;
            else
                currentState = InputState.Abort;

            Reset();
        }
        
        public void InvokeStart()
        {
            if (isAlreadyStarted) return;
            
            started?.Invoke();
            isAlreadyStarted = true;
        }

        public void InvokePerformed()
        {
            performed?.Invoke();
        }

        public void InvokeCanceled()
        {
            canceled?.Invoke();
            isAlreadyStarted = false;
            currentState = InputState.Idle;
        }
        
        public void InvokeEnded()
        {
            ended?.Invoke();
            isAlreadyStarted = false;
            currentState = InputState.Idle;
        }
        
        public void AddListener(InputAction phase, Action action)
        {
            switch (phase)
            {
                case InputAction.Started:
                    started += action;
                    break;
                case InputAction.Performed:
                    performed += action;
                    break;
                case InputAction.Ended:
                    ended += action;
                    break;
                case InputAction.Canceled:
                    canceled += action;
                    break;
                default:
                    Debug.LogError($"[{phase}]Nothing can be assigned to this phase");
                    break;
            }
        }

        public void RemoveListener(InputAction phase, Action action)
        {
            switch (phase)
            {
                case InputAction.Started:
                    started -= action;
                    break;
                case InputAction.Performed:
                    performed -= action;
                    break;
                case InputAction.Ended:
                    ended -= action;
                    break;
                case InputAction.Canceled:
                    canceled -= action;
                    break;
                default:
                    Debug.LogError($"[{phase}]Nothing can be removed from this phase");
                    break;
            }
        }
        
        protected Vector2 GetTouchPositionType(VectorType vectorType)
        {
            return devices.Where(device => device.IsTouchReceived())
                .Select(device => device.GetPositionType(vectorType))
                .FirstOrDefault();
        }

        protected bool IsTouchReceived()
        {
            return devices.Any(device => device.IsTouchReceived());
        }
    }

}