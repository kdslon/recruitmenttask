﻿using RT.ObjectPooling;

namespace RT.GameLogic
{
    public class LevelControllerFacade : ILevelControllerFacade
    {
        private readonly ILevelController levelController;
        
        public LevelControllerFacade(ILevelController levelController)
        {
            this.levelController = levelController;
        }

        public void InitializeController()
        {
            levelController.InitializeController();
        }

        public void DisposeController()
        {
            levelController.DisposeController();
        }

        public void AddEnemy(PoolableCharacter enemy)
        {
            levelController.AddEnemy(enemy);
        }

        public void AddAlly(PoolableCharacter character)
        {
            levelController.AddAlly(character);
        }
    }
}