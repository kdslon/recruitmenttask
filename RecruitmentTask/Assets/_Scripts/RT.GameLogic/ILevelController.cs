﻿using RT.ObjectPooling;

namespace RT.GameLogic
{
    public interface ILevelController
    {
        void InitializeController();
        void DisposeController();
        void AddEnemy(PoolableCharacter enemy);
        void AddAlly(PoolableCharacter character);
    }
}
