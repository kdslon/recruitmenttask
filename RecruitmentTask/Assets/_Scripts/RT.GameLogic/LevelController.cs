﻿using System.Collections.Generic;
using RT.Fire;
using RT.Input;
using RT.ObjectPooling;
using RT.Signal;
using RT.Utilities;
using UnityEngine;
using Zenject;

namespace RT.GameLogic
{
    public class LevelController : ILevelController
    {
        [Inject]
        private readonly IInputSystemFacade inputSystemFacade;

        [Inject]
        private readonly ISignalSystemFacade signalSystemFacade;
        
        private List<PoolableCharacter> allies = new List<PoolableCharacter>();
        private List<PoolableCharacter> enemies = new List<PoolableCharacter>();

        public void InitializeController()
        {
            signalSystemFacade.SubscribeSignal<OnKillSignal>(KillCharacter);
        }
        
        public void DisposeController()
        {
            signalSystemFacade.UnsubscribeSignal<OnKillSignal>(KillCharacter);
            ClearCharacters();
        }

        public void AddEnemy(PoolableCharacter enemy)
        {
            enemies.Add(enemy);
            enemy.GetMovementSystem().InitializeSystem(inputSystemFacade);
            enemy.GetLineDrawer().InitializeSystem(inputSystemFacade);
        }

        public void AddAlly(PoolableCharacter character)
        {
            allies.Add(character);
            character.GetMovementSystem().InitializeSystem(inputSystemFacade);
            character.GetLineDrawer().InitializeSystem(inputSystemFacade);
        }

        private void KillCharacter(OnKillSignal signalData)
        {
            if(signalData.characterFightSystem.GetCharacterType() == CharacterType.Ally)
                KillAlly(signalData.characterFightSystem);
            else
                KillEnemy(signalData.characterFightSystem);
        }

        private void KillAlly(BaseCharacterFightSystem allyFightSystem)
        {
            var ally = allies.Find(x => x.GetCharacterFightSystem().Equals(allyFightSystem));
            if (ally == null)
            {
                Debug.LogError("WrongFightSystem");
                return;
            }

            ally.GetMovementSystem().DisposeSystem();
            ally.GetLineDrawer().DisposeSystem(inputSystemFacade);
            ally.GetBoxCollider().enabled = false;
            allies.Remove(ally);
            RedrawLines();
            
            signalSystemFacade.FireSignal<OnLoseSignal>();
            ClearCharacters();
        }

        private void KillEnemy(BaseCharacterFightSystem enemyFightSystem)
        {
            var enemy = enemies.Find(x => x.GetCharacterFightSystem().Equals(enemyFightSystem));
            if (enemy == null)
            {
                Debug.LogError("WrongFightSystem");
                return;
            }
            
            enemy.GetMovementSystem().DisposeSystem();
            enemy.GetLineDrawer().DisposeSystem(inputSystemFacade);
            enemy.GetBoxCollider().enabled = false;
            enemies.Remove(enemy);
            RedrawLines();

            if (enemies.Count != 0) return;
            signalSystemFacade.FireSignal<OnWinSignal>();
            ClearCharacters();
        }

        private void ClearCharacters()
        {
            foreach (var enemy in enemies)
            {
                enemy.GetMovementSystem().DisposeSystem();
                enemy.GetLineDrawer().DisposeSystem(inputSystemFacade);
                enemy.GetBoxCollider().enabled = false;
            }

            foreach (var ally in allies)
            {
                ally.GetMovementSystem().DisposeSystem();
                ally.GetLineDrawer().DisposeSystem(inputSystemFacade);
                ally.GetBoxCollider().enabled = false;
            }

            enemies.Clear();
            allies.Clear();
        }

        private void RedrawLines()
        {
            foreach (var enemy in enemies)
            {
                enemy.GetLineDrawer().DrawBallisticLine();
            }

            foreach (var ally in allies)
            {
                ally.GetLineDrawer().DrawBallisticLine();
            }
        }
    }
}