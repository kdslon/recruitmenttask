﻿using RT.GameLogic;
using RT.Utilities;
using UnityEngine;
using Zenject;

namespace RT.Installer
{
    public class LevelControllerAsSingleInstaller : BaseAsSingleInstaller<ILevelControllerFacade, LevelControllerFacade>
    {
        [SerializeField] 
        private BindingMode levelController;
        
        protected override void InstallSystem(DiContainer subContainer)
        {
            base.InstallSystem(subContainer);
            BindContractAndSubstitutionWithModeAsSingle<ILevelController, LevelController>(levelController);
        }
    }
}