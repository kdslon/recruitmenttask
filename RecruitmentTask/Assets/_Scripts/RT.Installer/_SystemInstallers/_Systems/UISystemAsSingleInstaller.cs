﻿using RT.Utilities;
using RT.View;
using UnityEngine;
using Zenject;

namespace RT.Installer
{
    public class UISystemAsSingleInstaller : BaseAsSingleInstaller<IUIFacade, UIFacade>
    {
        [SerializeField] 
        private BindingMode mainMenuView;
        
        [SerializeField] 
        private BindingMode factPanel;

        [SerializeField] 
        private BindingMode gameView;
        
        protected override void InstallSystem(DiContainer subContainer)
        {
            base.InstallSystem(subContainer);
            BindContractAndSubstitutionWithModeFromResolve<IMainMenuView, MainMenuView>(mainMenuView);
            BindContractAndSubstitutionWithModeFromResolve<IFactPanel, FactPanel>(factPanel);
            BindContractAndSubstitutionWithModeFromResolve<IGameView, GameView>(gameView);
        }
    }
}