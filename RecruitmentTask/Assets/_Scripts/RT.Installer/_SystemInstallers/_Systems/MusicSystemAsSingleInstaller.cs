﻿using RT.Music;
using RT.Utilities;
using UnityEngine;
using Zenject;

namespace RT.Installer
{
    public class MusicSystemAsSingleInstaller : BaseAsSingleInstaller<IMusicSystemFacade, MusicSystemFacade>
    {
        [SerializeField] private BindingMode musicSystem;
        
        protected override void InstallSystem(DiContainer subContainer)
        {
            base.InstallSystem(subContainer);
            BindContractAndSubstitutionWithModeFromResolve<IMusicSystem, MusicSystem>(musicSystem);
        }
    }

}