﻿using RT.LevelGenerator;
using RT.ObjectPooling;
using RT.Utilities;
using UnityEngine;

namespace RT.Installer
{
    public class LevelGeneratorMonoBehaviourInstaller : BaseMonoBehaviourInstaller
    {
        [SerializeField]
        private BindingMode levelGeneratorSystemBinding;

        [SerializeField]
        private PoolableCharacter poolableAlly;
        
        [SerializeField]
        private PoolableCharacter poolableEnemy;
        
        [SerializeField]
        private PoolableWall poolableWall;
        

        [SerializeField] 
        private LevelGeneratorSystem levelGeneratorSystem;
        
        protected override void InstallSystem()
        {
            base.InstallSystem();
            BindContractAndSubstitutionWithModeFromInstance<ILevelGeneratorSystem, LevelGeneratorSystem>(levelGeneratorSystemBinding, levelGeneratorSystem);
            Container.BindMemoryPool<PoolableAlly, Pool<PoolableAlly>>().WithInitialSize(5).FromComponentInNewPrefab(poolableAlly).UnderTransformGroup("PoolableAllyPool"); 
            Container.BindMemoryPool<PoolableEnemy, Pool<PoolableEnemy>>().WithInitialSize(5).FromComponentInNewPrefab(poolableEnemy).UnderTransformGroup("PoolableEnemyPool");
            Container.BindMemoryPool<PoolableWall, Pool<PoolableWall>>().WithInitialSize(5).FromComponentInNewPrefab(poolableWall).UnderTransformGroup("PoolableWallPool");
            }
    }
}
