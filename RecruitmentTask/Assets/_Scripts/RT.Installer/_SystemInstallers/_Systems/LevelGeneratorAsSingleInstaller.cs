﻿using RT.LevelGenerator;
using RT.ObjectPooling;
using RT.Utilities;
using UnityEngine;
using Zenject;

namespace RT.Installer
{
    public class LevelGeneratorAsSingleInstaller : BaseAsSingleInstaller<ILevelGeneratorSystemFacade, LevelGeneratorSystemFacade>
    {

    }
}