﻿using RT.Input;
using RT.Utilities;
using UnityEngine;
using Zenject;

namespace RT.Installer
{
    public class InputSystemAsSingleInstaller : BaseAsSingleInstaller<IInputSystemFacade, InputSystemFacade>
    {
        [SerializeField]
        private BindingMode inputSystem;
        [SerializeField]
        private BindingMode mouse;
        [SerializeField]
        private BindingMode touchScreen;
        protected override void InstallSystem(DiContainer subContainer)
        {
            base.InstallSystem(subContainer);
            BindContractAndSubstitutionWithModeAsSingle<IInputDevice, TouchScreen>(touchScreen);
            BindContractAndSubstitutionWithModeAsSingle<IInputDevice, Mouse>(mouse);
            BindContractAndSubstitutionWithModeAsSingle<IInputSystem, InputSystem>(inputSystem);
        }
    }
}
