﻿using RT.Cam;
using RT.Utilities;
using UnityEngine;
using Zenject;

namespace RT.Installer
{
    public class BezierCameraSystemAsSingleInstaller : BaseAsSingleInstaller<IBezierCameraMovementSystemFacade, BezierCameraMovementSystemFacade>
    {
        [SerializeField] 
        private BindingMode bezierCam;
        
        protected override void InstallSystem(DiContainer subContainer)
        {
            base.InstallSystem(subContainer);
            BindContractAndSubstitutionWithModeFromResolve<IBezierCameraMovementSystem, BezierCameraMovementSystem>(bezierCam);
        }
    }
}