﻿using RT.Installer;
using RT.Utilities;
using UnityEngine;
using Zenject;

namespace RT.Signal
{
    public class SignalSystemAsSingleInstaller : BaseAsSingleInstaller<ISignalSystemFacade, SignalSystemFacade>
    {
        [SerializeField] 
        private BindingMode signalSystem;
        protected override void InstallSystem(DiContainer subContainer)
        {
            base.InstallSystem(subContainer);
            SignalBusInstaller.Install(subContainer);
            BindContractAndSubstitutionWithModeAsSingle<ISignalSystem<CustomSignal>, SignalSystem>(signalSystem);
            DeclareSignals();
        }

        private void DeclareSignals()
        {
            subContainer.DeclareSignal<OnButtonClickedSignal>();
            subContainer.DeclareSignal<OnSwitchClickedSignal>();
            subContainer.DeclareSignal<OnMuteClickedSignal>();
            subContainer.DeclareSignal<OnStartGameSignal>();
            subContainer.DeclareSignal<OnDifficultyClickedSignal>();
            subContainer.DeclareSignal<OnGenerateLevelSignal>();
            subContainer.DeclareSignal<OnShootSignal>();
            subContainer.DeclareSignal<OnKillSignal>();
            subContainer.DeclareSignal<OnWinSignal>();
            subContainer.DeclareSignal<OnNextLevelSignal>();
            subContainer.DeclareSignal<OnLoseSignal>();
            subContainer.DeclareSignal<OnRestartLevelSignal>();
        }
    }
}
