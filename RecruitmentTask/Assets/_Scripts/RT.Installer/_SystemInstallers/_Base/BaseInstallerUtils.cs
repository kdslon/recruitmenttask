﻿using Zenject;
using NSubstitute;
using RT.Utilities;

namespace RT.Installer
{
    public class BaseInstallerUtils : MonoInstaller
    {
        protected DiContainer subContainer;
        
        protected void BindContractAndSubstitutionWithModeFromInstance<TContract, TConcrete>(BindingMode bindingMode, TConcrete instance)
            where TConcrete : TContract
            where TContract : class
        {
            if (bindingMode.Equals(BindingMode.Concrete))
                subContainer.Bind<TContract>().To<TConcrete>().FromInstance(instance);
            else
                subContainer.Bind<TContract>().FromInstance(Substitute.For<TContract>());
        }

        protected void BindContractAndSubstitutionWithModeAsSingle<TContract, TConcrete>(BindingMode bindingMode)
            where TConcrete : TContract
            where TContract : class
        {
            if (bindingMode.Equals(BindingMode.Concrete))
                subContainer.Bind<TContract>().To<TConcrete>().AsSingle();
            else
                subContainer.Bind<TContract>().FromInstance(Substitute.For<TContract>());
        }

        protected void BindContractAndSubstitutionWithModeFromResolve<TContract, TConcrete>(BindingMode bindingMode)
            where TConcrete : TContract
            where TContract : class
        {
            if (bindingMode.Equals(BindingMode.Concrete))
                subContainer.Bind<TContract>().To<TConcrete>().FromResolve();
            else
                subContainer.Bind<TContract>().FromInstance(Substitute.For<TContract>());
        }
    }
}
