﻿namespace RT.Installer
{
    public class BaseMonoBehaviourInstaller : BaseInstallerUtils
    {
        public override void InstallBindings()
        {
            InstallSystem();
        }

        protected virtual void InstallSystem()
        {
            this.subContainer = Container;
        }
    }
}
