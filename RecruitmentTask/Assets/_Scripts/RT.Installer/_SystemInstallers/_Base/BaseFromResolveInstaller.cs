﻿using RT.Utilities;
using UnityEngine;
using Zenject;

namespace RT.Installer
{
    public class BaseFromResolveInstaller<TContractFacade, TConcreteFacade> : BaseInstallerUtils
        where TConcreteFacade : TContractFacade
        where TContractFacade : class
    {
        [SerializeField]
        protected BindingMode facade;

        public override void InstallBindings()
        {
            Container.Bind<TContractFacade>().FromSubContainerResolve().ByMethod(InstallSystem).AsSingle();
        }

        protected virtual void InstallSystem(DiContainer subContainer)
        {
            this.subContainer = subContainer;
            BindContractAndSubstitutionWithModeFromResolve<TContractFacade, TConcreteFacade>(facade); 
        }
    }
}
