﻿using System.Collections;
using System.Collections.Generic;
using RT.Utilities;
using UnityEngine;
using Zenject;

namespace RT.Installer
{
    public class BaseAsSingleInstaller<TContractFacade, TConcreteFacade> : BaseInstallerUtils
        where TConcreteFacade : TContractFacade
        where TContractFacade : class
    {
        protected BindingMode facade;

        public override void InstallBindings()
        {
            Container.Bind<TContractFacade>().FromSubContainerResolve().ByMethod(InstallSystem).AsSingle();
        }

        protected virtual void InstallSystem(DiContainer subContainer)
        {
            this.subContainer = subContainer;
            BindContractAndSubstitutionWithModeAsSingle<TContractFacade, TConcreteFacade>(facade);
        }
    } 
}
