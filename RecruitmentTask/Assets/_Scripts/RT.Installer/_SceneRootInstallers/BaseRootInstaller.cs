﻿using RT.States;
using Zenject;

namespace RT.Installer
{
    public class BaseRootInstaller<TRoot> : MonoInstaller where TRoot : IRoot
    {
        public override void InstallBindings()
        {
            BindReferences();
        }

        private void BindReferences()
        {
            Container.BindInterfacesTo<TRoot>().AsSingle();
            Container.Bind<ISceneRootFacade>().To<SceneRootFacade>().AsSingle();
        }
    }
}